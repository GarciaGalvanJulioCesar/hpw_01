//enterno global
var x = 10

//enterno global

function f(y) {
    // var solo tiene sentido dentro de una funcion para que no 
   // pertenesca al enterno global
  var  x = 5;
    
    y *= x;
    //enterno global
    
    console.log(y);
   
}

//enternoglobal

f(x);

//enterno global

// el enterno objeto golb